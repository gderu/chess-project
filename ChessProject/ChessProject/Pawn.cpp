#include "Pawn.h"
#define FIRST 0
#define TWO_SQUARES 2
#define ONE_SQUARE 1
#define LEFT 1
#define RIGHT -1
#define HAVENT_MOVED 0
#define MOVED_ONCE 1

Pawn::Pawn(std::string loc, bool color): Piece(loc, color), canCaptureLeft(false), canCaptureRight(false)
{
}

void Pawn::updateCanCaptureLeft(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo)
{
	unsigned char y = moveTo[Y] - '1', x = moveTo[X] - 'a';
	if (LEFT == this->_loc[X] - moveTo[X]) {
		if (boardRep[y][x] != nullptr && boardRep[y][x]->getColor() != this->_color)
		{
			this->canCaptureLeft = true;
		}
		else
		{
			canCaptureLeft = canEnPassent(boardRep, moveTo, false);
		}
	}
	else {
		this->canCaptureLeft = false;
	}
}
//TODO: add 1 block Y movment
void Pawn::updateCanCaptureRight(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo)
{
	unsigned char y = moveTo[Y] - '1', x = moveTo[X] - 'a';
	if (RIGHT == this->_loc[X] - moveTo[X] ) {
		if (boardRep[y][x] != nullptr && boardRep[y][x]->getColor() != this->_color)
		{
			this->canCaptureRight = true;
		}
		else
		{
			canCaptureRight = canEnPassent(boardRep, moveTo, true);
		}
	}
	else
	{
		canCaptureRight = false;
	}


}

bool Pawn::movingLegal(int amount, int goalY, int origY)
{
	return (goalY - origY == amount && true == this->_color) || //if its white and moving up
		(origY - goalY == amount && false == this->_color);//if its black and moving down
}
//TODO: add && boardRep[y - 1][x]->getMoveNum() == FIRST + 1 and && boardRep[y + 1][x]->getMoveNum() == FIRST + 1 
bool Pawn::canEnPassent(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo, bool right)
{
	unsigned char y = moveTo[Y] - '1', x = moveTo[X] - 'a';
	unsigned char currY = _loc[Y] - '1', currX = _loc[X] - 'a';
	
	if (right  && currX + 1 < BOARD_SIZE && boardRep[currY][currX + 1] != nullptr  &&_color != boardRep[currY][currX + 1]->getColor() &&
		boardRep[currY][currX + 1] == _lastMoved && boardRep[currY][currX + 1]->getType() == 'p' && currX- x != 0)
	{
		return true;

	}
	else if (currX - 1 > 0 && boardRep[currY][currX - 1] != nullptr && _color != boardRep[currY][currX - 1]->getColor() &&
		boardRep[currY][currX - 1] == _lastMoved && boardRep[currY][currX - 1]->getType() == 'p'&& currX - x != 0 )
	{

		return true;
	}
	else
	{
		return false;
	}
}
//this function checks if there is a piece in the way.
//input: the board, string to move the this to.
//output: true if there is a piece in the way.
bool Pawn::isPieceInWay(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo)
{
	int origY = (int)(this->_loc[Y] - '1'), goalY = (int)(moveTo[Y] - '1');
	updateCanCaptureLeft(boardRep, moveTo);
	updateCanCaptureRight(boardRep, moveTo);
	if (boardRep[goalY][(int)(moveTo[X] - 'a')] != nullptr && (abs(moveTo[X] - _loc[X]) == 0 ? true: boardRep[goalY][(int)(moveTo[X] - 'a')]->getColor() == _color ? true:false)) {
		return true;
	}
	if (abs(moveTo[Y] - _loc[Y]) != ONE_SQUARE && boardRep[this->_color ? origY + ONE_SQUARE : origY - ONE_SQUARE][(int)(moveTo[X] - 'a')] != nullptr) {
		return true;
	}
	return false;
}

	
//this function checks if the move is ilegal (eg rook not moving in stright lines, bishop not zigzagging etc)
//input: where to move this to
//output: true if the move is illegal
bool Pawn::illegalMove(std::string moveTo)
{
	int origY = (int)(this->_loc[Y] - '1'), goalY = (int)(moveTo[Y] - '1');
	if(this->_loc[X] == moveTo[X] && (     (FIRST == this->getMoveNum() && movingLegal(TWO_SQUARES, goalY, origY)) || 
		movingLegal(ONE_SQUARE, goalY, origY)    )){//if its black and moving down
		return false;
	}
	if (movingLegal(ONE_SQUARE, goalY, origY) && // if its black and moving down
		((this->_loc[X] - moveTo[X] == RIGHT && this->canCaptureRight) || //the piece is capturing to the left
		(this->_loc[X] - moveTo[X] == LEFT && this->canCaptureLeft))) {//the piece is capturing to the right
		return false;
	}
	return true;
}
//this function sets the loc of the piece, deletes other pieces if neccessary.
//input: where to move the pice, the board.
void Pawn::setLoc(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE])
{
	unsigned char y = _loc[Y] - '1', x = _loc[X] - 'a';
	bool enPassent = canEnPassent(board, moveTo, moveTo[X] - _loc[X] > 0);
	if (moveTo[X] - _loc[X] > 0 && enPassent)//The eaten must be black and below
	{
		delete board[y][x + 1];
		board[y][x + 1] = nullptr;
	}
	else if (enPassent)//the eaten is white and above
	{
		delete board[y][x - 1];
		board[y][x - 1] = nullptr;
	}
	Piece::setLoc(moveTo, board);
	this->setMoveNum(this->getMoveNum() + MOVED_ONCE);
}
//this function changes the loc but doesn't delete pieces.
//input: where to move the piece, the board, what to put in the original location.
Piece* Pawn::setLocNoDel(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE], Piece * replaceWith)
{
	unsigned char y = _loc[Y] - '1', x = _loc[X] - 'a';
	return Piece::setLocNoDel(moveTo, board, replaceWith);
}
//this function returns the type of the piece, eg b for bishop

char Pawn::getType() const
{
	return 'p';
}
