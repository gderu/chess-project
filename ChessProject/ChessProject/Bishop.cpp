#include "Bishop.h"


//ctor
Bishop::Bishop(std::string loc, bool color): Piece(loc, color)
{
}
//this function checks if there is a piece in the way.
//input: the board, string to move the this to.
//output: true if there is a piece in the way.
bool Bishop::isPieceInWay(Piece* boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo)
{
	int diffX = this->_loc[X] < moveTo[X] ? 1 : -1, diffY = this->_loc[Y] < moveTo[Y] ? 1 : -1;
	int x = (int)(this->_loc[X] - 'a'), y = (int)(this->_loc[Y] - '1');
		if (boardRep[(int)(moveTo[Y] - '1')][(int)(moveTo[X] - 'a')] != nullptr && boardRep[(int)(moveTo[Y] - '1')][(int)(moveTo[X] - 'a')]->getColor() == this->_color) {
		return true;
	}
	for (x += diffX, y += diffY; x != (int)(moveTo[X] - 'a') && y != (int)(moveTo[Y] - '1'); x += diffX, y += diffY) {
		if (boardRep[y][x] != nullptr) {
			return true;
		}
	}
	return false;
}
//this function checks if the move is ilegal (eg rook not moving in stright lines, bishop not zigzagging etc)
//input: where to move this to
//output: true if the move is illegal
bool Bishop::illegalMove(std::string moveTo)
{
	int diffX = abs(this->_loc[X] - moveTo[X]), diffY = abs(this->_loc[Y] - moveTo[Y]);
	return diffX != diffY;
}
//this function returns the type of the piece, eg b for bishop
char Bishop::getType() const
{
	return 'b';
}
