#pragma once
#define BOARD_SIZE 8
#include "Rook.h"
#include "King.h"
#include "Queen.h"
#include "Knight.h"
#include "Bishop.h"
#include "Pawn.h"
class Test
{
	Piece * _boardRep[BOARD_SIZE][BOARD_SIZE];
public:
	Test();
	~Test();
	void cleanBoard();
	void printBoard();
	void testPawn();
	void testQueen();
	void testBishop();
	void testPiece();
	void testRook();
	void testKing();
	void testKnight();
};

