#include "Knight.h"

//ctor
Knight::Knight(std::string loc, bool color): Piece(loc, color)
{
}
//this function checks if there is a piece in the way.
//input: the board, string to move the this to.
//output: true if there is a piece in the way.
bool Knight::isPieceInWay(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo)
{
	if (boardRep[moveTo[Y] - '1'][moveTo[X] - 'a'] != nullptr && boardRep[moveTo[Y] - '1'][moveTo[X] - 'a']->getColor() == this->_color)
	{
		int i = 0;
	}
	return (boardRep[moveTo[Y] - '1'][moveTo[X] - 'a'] != nullptr) &&  (boardRep[moveTo[Y] - '1'][moveTo[X] - 'a']->getColor() == this->_color );
}
//this function checks if the move is ilegal (eg rook not moving in stright lines, bishop not zigzagging etc)
//input: where to move this to
//output: true if the move is illegal
bool Knight::illegalMove(std::string moveTo)
{
	return !(    (KNIGHT_MIN_MOV == abs(moveTo[Y] - _loc[Y]) || KNIGHT_MIN_MOV == abs(moveTo[X] - _loc[X])) && 
		(KNIGHT_MAX_MOV == abs(moveTo[Y] - _loc[Y]) || KNIGHT_MAX_MOV == abs(moveTo[X] - _loc[X]))      );
}
//this function returns the type of the piece, eg b for bishop
char Knight::getType() const
{
	return 'n';
}
