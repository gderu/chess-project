#pragma once
#include "Bishop.h"
#include "Rook.h"
class Queen : virtual public Rook, virtual public Bishop
{
public:
	Queen(std::string loc, bool color);
	bool illegalMove(std::string moveTo); //returns true if the move is ilegal
	bool isPieceInWay(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo);
	virtual char getType() const;
};

