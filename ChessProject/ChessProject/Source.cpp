#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Test.h"
#include "Board.h"
#define NUM_TO_CHANGE 0
#define LEN_TO_SEND 2
#define MOVE_TO 2
using namespace std;
void main()
{
	srand(time_t(NULL));

	/*Test t;
	t.printBoard();
	cout << "Rook" << endl;
	t.testRook();
	cout << "Bishop" << endl;
	t.testBishop();
	cout << "King" << endl;
	t.testKing();
	cout << "Knight" << endl;
	t.testKnight();
	cout << "Piece" << endl;
	t.testPiece();
	cout << "Queen" << endl;
	t.testQueen();
	cout << "Pawn" << endl;
	t.testPawn();*/

	Pipe p;
	Board b;
	bool isConnect = p.connect();
	char msgTemplate[2] = { 0 };
	std::string original = "00", moveTo = "00";
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if ("0" == ans)
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		system("CLS");
		for (int i = 0; i < LEN_TO_SEND; i++) {
			original[i] = msgFromGraphics[i];
			moveTo[i] = msgFromGraphics[i + MOVE_TO];
		}
		msgTemplate[NUM_TO_CHANGE] = b.responseFinder(original, moveTo);
		strcpy_s(msgToGraphics, msgTemplate); // msgToGraphics should contain the result of the operation



		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   
		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();

	}

	p.close();

	return;
}