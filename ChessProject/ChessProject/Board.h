#pragma once
#include "Piece.h"
#include <vector>
#include <iostream>
#include "Pawn.h"
#include "Bishop.h"
#include "Knight.h"
#include "King.h"
#include "Queen.h"
#include "Rook.h"
#define BOARD_SIZE 8
#define X 0
#define Y 1

class Board
{
	Piece * _board[BOARD_SIZE][BOARD_SIZE];
	Piece * _kingWhite;
	Piece * _kingBlack;
	void initCol(bool color, std::string col);
	void initPawns(bool color, unsigned char col);
	bool checkChecker(bool colorThreating, std::string loc, std::string moveTo);
	Piece *getPiece(std::string place);
	bool _turn;
public:
	Board();
	~Board();
	//no need for = operator, no pointers.
	bool canPromotion(std::string loc) const { return _board[loc[Y] - '1'][loc[X] - 'a'] != nullptr && _board[loc[Y] - '1'][loc[X] - 'a']->getType() == 'p' && ((loc[Y] - '1') == (BOARD_SIZE - 1) || (loc[Y] - '1') == 0); };// i know its not nullptr from other check
	void promotion(std::string loc);
	void printBoard(Piece * board[BOARD_SIZE][BOARD_SIZE]) const;
	bool accidentalCheck(std::string loc, std::string moveTo);
	bool purposefullCheck(std::string loc, std::string moveTo);
	char responseFinder(std::string loc, std::string moveTo);
	bool sameTile(std::string loc, std::string moveTo) const;
	bool outOfBounds(std::string moveTo);
	bool rightTurn(std::string loc);
};