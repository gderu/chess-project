#pragma once
#include "Piece.h"
#include <iostream>
#define KNIGHT_MAX_MOV 2
#define KNIGHT_MIN_MOV 1
class Knight: public Piece
{
public:
	Knight(std::string loc, bool color);
	//no need for = operator, no pointers
	virtual bool isPieceInWay(Piece* boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo);
	virtual bool illegalMove(std::string moveTo);
	virtual char getType() const;
};

