#include "Rook.h"
//ctor
Rook::Rook(std::string loc, bool color): Piece(loc, color)
{
}
//returns if the move is ilegal
//returns true if illegal, false if legal
//gets the destination
bool Rook::illegalMove(std::string moveTo)
{
	if (_loc[X] != moveTo[X] && _loc[Y]!= moveTo[Y])
	{
		return true;
	}
	return false;
}

//this function checks if there is a piece in the way.
//input: the board, string to move the this to.
//output: true if there is a piece in the way.
bool Rook::isPieceInWay(Piece * _boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo)
{
	unsigned char axis = _loc[Y] == moveTo[Y] ? X : Y;
	unsigned char start = _loc[axis] < moveTo[axis] ? _loc[axis] : moveTo[axis];//start low
	unsigned char end = _loc[axis] > moveTo[axis] ? _loc[axis] : moveTo[axis];//end high
	if (X == axis)
	{
		start -= 'a';
		end -= 'a';
	}
	else
	{
		start -= '1';
		end -= '1';
	}
	for (unsigned char i = start + 1; i <end; i++)
	{	
		if ((X == axis && _boardRep[_loc[Y] - '1'][i] != nullptr) ||
			(Y == axis && _boardRep[i][_loc[X] - 'a'] != nullptr))//if there is a piece
		{
				return true;
		}
	}
	if (_boardRep[moveTo[Y] - '1'][moveTo[X] - 'a'] !=nullptr &&
		_boardRep[moveTo[Y] - '1'][moveTo[X] - 'a']->getColor() == this->getColor())
	{
		return true;//if the piece at the end is of the same team as this, the move is illegal
	}
	return false;
}
//this function returns the type of the piece, eg b for bishop

char Rook::getType() const
{
	return 'r';
}
