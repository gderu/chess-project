#include "Piece.h"
Piece * Piece::_lastMoved = nullptr;

#define HAVENT_MOVED 0

Piece::Piece(std::string loc, bool color) : _color(color), _loc(loc), moveNum(HAVENT_MOVED)
{
}


Piece::~Piece()
{
}


unsigned char Piece::getMoveNum()
{
	return this->moveNum;
}

void Piece::setMoveNum(unsigned char newMoveNum)
{
	this->moveNum = newMoveNum;
}
//this function sets the loc of the piece, deletes other pieces if neccessary.
//input: where to move the pice, the board.
void Piece::setLoc(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE])
{
	int y = (int)(moveTo[Y] - '1'), x = (int)(moveTo[X] - 'a');
	if (board[y][x] != nullptr)
	{
		Piece *t = board[y][x];
		delete board[y][x];
	}
 	board[y][x] = this;
	y = (int)(this->_loc[Y] - '1'), x = (int)(this->_loc[X] - 'a');
	board[y][x] = nullptr;
	this->_loc = moveTo;
}
//this function changes the loc but doesn't delete pieces.
//input: where to move the piece, the board, what to put in the original location.
//output: the piece that was at moveto
Piece * Piece::setLocNoDel(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE], Piece * replaceWith)
{
	Piece * toReturn = nullptr;
	int y = (int)(moveTo[Y] - '1'), x = (int)(moveTo[X] - 'a');
	toReturn = board[y][x];
	board[y][x] = this;
	y = (int)(this->_loc[Y] - '1'), x = (int)(this->_loc[X] - 'a');
	board[y][x] = replaceWith;
	this->_loc = moveTo;
	return toReturn;
}

std::string Piece::getLoc() const
{
	return this->_loc;
}
