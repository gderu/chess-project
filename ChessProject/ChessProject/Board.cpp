#include "Board.h"
#define NO_PIECES_TAKEN "-1"

//this function initiats a colum with varius pieces/
//input: color (true for white), the colum.
void Board::initCol(bool color, std::string col)
{
	enum {
		ROOK,
		KNIGHT,
		BISHOP,
		KING,
		QUEEN
	};
	_board[col[0] - '1'][KING] = new King("d" + col, color);
	if (color)
	{
		_kingWhite = _board[col[0] - '1'][KING];
	}
	else
	{
		_kingBlack = _board[col[0] - '1'][KING];
	}

	_board[col[0] - '1'][QUEEN] = new Queen("e" + col, color);

	_board[col[0] - '1'][0] = new Rook("a" + col, color);
	_board[col[0] - '1'][BOARD_SIZE - 1] = new Rook("h" + col, color);

	_board[col[0] - '1'][KNIGHT] = new Knight("b" + col, color);
	_board[col[0] - '1'][BOARD_SIZE - KNIGHT - 1] = new Knight("g" + col, color);

	_board[col[0] - '1'][BISHOP] = new Bishop("c" + col, color);
	_board[col[0] - '1'][BOARD_SIZE - BISHOP - 1] = new Bishop("f" + col, color);
}
//this function inits the pawns of a single line.
//input: the color (true for white), the colum 
void Board::initPawns(bool color, unsigned char col)
{
	char arr[Y + 2] = { 0 };
	for (unsigned char i = 0; i < BOARD_SIZE; i++)//creating a row of pawns
	{
		arr[X] = (char)(i + 'a');
		arr[Y] = (char)(col + '1');
		_board[col][i] = new Pawn(std::string(arr), color);//TO DO: add vriables to PAWN
	}
}
//ctor
Board::Board()
{
	_turn = true;//black starts
	//creating the pices.
	initCol(true, std::string("1"));//first row
	initPawns(true, 1);
	for (unsigned char i = 2; i < BOARD_SIZE - 2; i++)
	{
		for (unsigned char k = 0; i < BOARD_SIZE; i++)
		{
			_board[i][k] = nullptr;
		}
	}
	initCol(false, std::string("8"));
	initPawns(false, 6);
}
//this function allows promotion of a pawn.
//input: the location of the piece t obe promoted
void Board::promotion(std::string loc)
{
	enum {
		ROOK,
		KNIGHT,
		BISHOP,
		QUEEN,
		FAIL
	};
	unsigned char changeTo = FAIL;
	unsigned char y = loc[Y] - '1';
	unsigned char x = loc[X] - 'a';
	Piece * oldPiece = _board[y][x];
	Piece * newPiece = nullptr;
	using namespace std;
	cout << "what would you like to print?" << endl <<
		"0 - ROOK" << endl <<
		"1 - KNIGHT" << endl <<
		"2 - BISHOP" << endl <<
		"3 - QUEEN" << endl;
	while (changeTo == FAIL)
	{
		std::cin >> changeTo;
		changeTo -= '0';
		if (cin.fail())
		{
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
			changeTo = FAIL;
		}
		else if ( changeTo > QUEEN)//changeTo < ROOK can't be true since changeTo is unsigned
		{
			changeTo = FAIL;
			cout << "Invalid digit. Try again";
		}
	}
	switch (changeTo)
	{
	case ROOK:
		newPiece = new Rook(loc, oldPiece->getColor());
		delete _board[y][x];
		_board[y][x] = newPiece;
		break;
	case KNIGHT:
		newPiece = new Knight(loc, oldPiece->getColor());
		delete _board[y][x];
		_board[y][x] = newPiece;
		break;
	case BISHOP:
		newPiece = new Bishop(loc, oldPiece->getColor());
		delete _board[y][x];
		_board[y][x] = newPiece;
		break;
	case QUEEN:
		newPiece = new Queen(loc, oldPiece->getColor());
		delete _board[y][x];
		_board[y][x] = newPiece;
		break;
	}
}
//this function print the board/
//input: the board.
void Board::printBoard(Piece * board[BOARD_SIZE][BOARD_SIZE]) const
{
	using namespace std;
	for (char i = BOARD_SIZE - 1; i >=0; i--)
	{
		for (unsigned char k = 0; k < BOARD_SIZE; k++)
		{
			if (nullptr == board[i][k])
			{
				cout << " X  ";
			}
			else
			{
				cout << " " << board[i][k]->getColor() << board[i][k]->getType() << " ";
			}
		}
		cout << endl;
	}
}

//dtor
Board::~Board()
{
	unsigned char len = BOARD_SIZE;
	for (unsigned char i = 0; i < len; i++) {
		for (unsigned char k = 0; k < len; k++)
		{
			if (_board[i][k] != nullptr)
			{
				delete _board[i][k];
			}
		}
	}
}
//this function checks if a chess occoured. It is universal and can be given any side to check.
//input: the color to check if can eat the king, the loc of the piece to move, where to move it
bool Board::checkChecker(bool colorThreating, std::string loc, std::string moveTo)
{
	bool isCheck = false;
	std::string locInBoard = "";
	bool deletedPiece = false;
	Piece * whatWasThere = nullptr;
	std::string kingLoc;

	whatWasThere = (this->_board)[loc[Y] - '1'][loc[X] - 'a']->setLocNoDel(moveTo, this->_board, nullptr);
	if (colorThreating)
	{
		kingLoc = _kingBlack->getLoc();
	}
	else
	{
		kingLoc = _kingWhite->getLoc();
	}
	for (unsigned char i = 0; i < BOARD_SIZE; i++)
	{
		for (unsigned char k = 0; k < BOARD_SIZE; k++)
		{
			if ((this->_board)[i][k] != nullptr && (this->_board)[i][k]->getColor() == colorThreating)
			{
				if (!(this->_board)[i][k]->isPieceInWay(this->_board, kingLoc) && !(this->_board)[i][k]->illegalMove(kingLoc))//is there an enemy piece that can reach teh kings.
				{
					isCheck = true;
					break;
				}
			}
		}
	}
	(this->_board)[moveTo[Y] - '1'][moveTo[X] - 'a']->setLocNoDel(loc, this->_board, whatWasThere);
	return isCheck;
}
//this function returns a piece at an inputed loc.
Piece * Board::getPiece(std::string place)
{
	return _board[place[Y] - '1'][place[X] - 'a'];
}
//this function checks if an accidental check was made. 
//input: the loc of the piece selected to move and where to move it.
//input: returns true if there is an accidental check.
bool Board::accidentalCheck(std::string loc, std::string moveTo)
{
	return checkChecker(! _turn, loc, moveTo);
}
//this function checks if a check was made. 
//input: the loc of the piece selected to move and where to move it.
//returns true if there is a check
bool Board::purposefullCheck(std::string _loc, std::string moveTo)
{
	return checkChecker( _turn, _loc, moveTo);
}
//this function finds what to return to the gui
//input: the loc of the piece to move, where to move it
//output: the response to the server
char Board::responseFinder(std::string loc, std::string moveTo)
{
	Piece * checkThis = _board[loc[Y] - '1'][loc[X] - 'a'];
	char response = '0';

	if (outOfBounds(moveTo))
	{
		 response = '5';
	}
	else if (sameTile(loc, moveTo))
	{
		 response = '7';
	}
	else if (nullptr == checkThis)
	{
		 response = '2';
	}
	else if (checkThis->isPieceInWay(_board, moveTo))
	{
		response = '3';
	}
	else if (checkThis->illegalMove(moveTo) || _turn != checkThis->getColor())
	{
		 response = '6';
	}
	else if (accidentalCheck(loc, moveTo))
	{
		 response = '4';
	}
	else if (purposefullCheck(loc, moveTo))
	{
		 response = '1';
	}
	if ('0' ==response || '1' == response)
	{
		_turn = !_turn;
		checkThis->setLoc(moveTo, _board);
		if (canPromotion(moveTo))
		{
			promotion(moveTo);
		}
		checkThis->setAsLast();
		printBoard(_board);
	}
	return response;
}
//checks if the piece is at the same tile. (doesn't move)
bool Board::sameTile(std::string loc, std::string moveTo) const
{
	return loc == moveTo;
}
//checks if the piece is moved outside the bounds of the board.
bool Board::outOfBounds(std::string moveTo)
{
	if (moveTo[X] > 'h' || moveTo[X] < 'a' || moveTo[Y] > '8' || moveTo[Y] < '1') {
		return true;
	}
	return false;
}
//checks if the right player plays.
bool Board::rightTurn(std::string loc)
{
	return _board[loc[Y] - '1'][loc[X] - 'a']->getColor() == _turn;
}
