#include "Queen.h"

//ctor
Queen::Queen(std::string loc, bool color): Rook(loc,color), Bishop(loc, color), Piece(loc,color)
{
}
//this function checks if the move is ilegal (eg rook not moving in stright lines, bishop not zigzagging etc)
//input: where to move this to
//output: true if the move is illegal
bool Queen::illegalMove(std::string moveTo)
{
	return Rook::illegalMove(moveTo) && Bishop::illegalMove(moveTo);
}
//this function checks if there is a piece in the way.
//input: the board, string to move the this to.
//output: true if there is a piece in the way.
bool Queen::isPieceInWay(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo)
{
	if (!Rook::illegalMove(moveTo)) {
		return Rook::isPieceInWay(boardRep, moveTo);
	}
	else {
		return Bishop::isPieceInWay(boardRep, moveTo);
	}
}
//this function returns the type of the piece, eg b for bishop

char Queen::getType() const
{
	return 'q';
}
