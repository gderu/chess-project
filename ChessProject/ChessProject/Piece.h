#pragma once
#include <string>
#include <iostream>
#include <vector>

#define BOARD_SIZE 8
#define X 0
#define Y 1
#define DOESNTE_EXIST -1
class Piece
{
	unsigned char moveNum;
protected:
	std::string _loc;
	bool _color;//true means white
	static Piece * _lastMoved;
public:
	Piece(std::string loc, bool color);
	void setAsLast() { _lastMoved = this; };
	unsigned char getMoveNum();
	void setMoveNum(unsigned char newMoveNum);
	virtual ~Piece();
	virtual Piece * setLocNoDel(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE], Piece * replaceWith);
	virtual void setLoc(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE]);
	std::string getLoc() const;
	bool getColor() const { return _color; };
	virtual char getType() const = 0;
	virtual bool isPieceInWay(Piece* boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo) = 0;
	virtual bool illegalMove(std::string moveTo) = 0;
};
