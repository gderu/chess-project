#include "King.h"
#define HAVENT_MOVED 0
//this function performs castling.
//input: the board, wehre to move the piece.
void King::castling(Piece * board[BOARD_SIZE][BOARD_SIZE], std::string moveTo)
{
	std::string rookPos, posOneBetween, posTwoBetween, posThreeBetween = "", col;
	Piece * t;
	col = moveTo[Y];
	if (("8" == col || "1" == col) && moveTo[Y] == this->getLoc()[Y]) {
		if ('b' == moveTo[X]) {
			rookPos = "a" + col;
			posOneBetween = "b" + col;
			posTwoBetween = "c" + col;
		}
		else if ('f' == moveTo[X]){
			rookPos = "h" + col;
			posOneBetween = "g" + col;
			posTwoBetween = "f" + col;
			posThreeBetween = "e" + col;
		}
		else {
			return;
		}
		t = board[posOneBetween[X] - 'a'][posOneBetween[Y] - '1'];
		if (this->getMoveNum() == HAVENT_MOVED){//the king hasn't moved
			if (board[rookPos[Y] - '1'][rookPos[X] - 'a'] != nullptr && board[rookPos[Y] - '1'][rookPos[X] - 'a']->getMoveNum() == HAVENT_MOVED) { //checks if rook hasnt moved
				if (board[posOneBetween[Y] - '1'][posOneBetween[X] - 'a'] == nullptr) {
					if (board[posTwoBetween[Y] - '1'][posTwoBetween[X] - 'a'] == nullptr) {
						if ("" == posThreeBetween || board[posThreeBetween[Y] - '1'][posThreeBetween[X] - 'a'] == nullptr) {//checks if there is nothing between the rook and the king
							this->isCastling = true;
						}
					}
				}
			}
		}
	}
}
//ctor
King::King(std::string loc, bool color):Piece(loc, color), isCastling(false), changeRook(false)
{
}
//this function checks if the move is ilegal (eg rook not moving in stright lines, bishop not zigzagging etc)
//input: where to move this to
//output: true if the move is illegal
bool King::illegalMove(std::string moveTo)
{
	if (this->isCastling) {
		this->changeRook = true;
		this->isCastling = false;
		return false;
	}
	return abs(_loc[X] - moveTo[X] )> 1 || abs(_loc[Y] - moveTo[Y]) > 1;//if any is bigger than one the king moves too much
}
//this function checks if there is a piece in the way.
//input: the board, string to move the this to.
//output: true if there is a piece in the way.
bool King::isPieceInWay(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo)
{
	this->castling(boardRep, moveTo);
	return (nullptr == boardRep[moveTo[Y] - '1'][moveTo[X] - 'a'] ? false : (boardRep[moveTo[Y] - '1'][moveTo[X] - 'a']->getColor() != this->getColor() ? false : true));//if its nullptr then thre is nothing there, if there is and its the enemie's color its legal, else illegal
}
//this function returns the type of the piece, eg b for bishop
char King::getType() const
{
	return 'k';
}
//note: this function has changes to work for king
//this function sets the loc of the piece, deletes other pieces if neccessary.
//input: where to move the pice, the board.
void King::setLoc(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE])
{
	std::string toFind = moveTo, moveRookTo = moveTo;
	if (this->changeRook) {
		this->changeRook = false;
		if ('b' == moveTo[X]) {
			toFind[X] = 'a';
			moveRookTo[X] = 'c';
		}
		else {
			toFind[X] = 'h';
			moveRookTo[X] = 'e';
		}
		board[toFind[Y] - '1'][toFind[X] - 'a']->setLoc(moveRookTo, board);
	}
	Piece::setLoc(moveTo, board);
}

