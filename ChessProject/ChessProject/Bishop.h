#pragma once
#include "Piece.h"
class Bishop : virtual public Piece
{
public:
	//no need for = operator, no pointers
	Bishop(std::string loc, bool color);
	virtual bool isPieceInWay(Piece* boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo);
	virtual bool illegalMove(std::string moveTo);
	virtual char getType() const;
};

