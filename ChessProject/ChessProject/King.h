#pragma once
#include "Piece.h"
class King: public Piece
{
private:
	bool isCastling;
	bool changeRook;
	void castling(Piece * board[BOARD_SIZE][BOARD_SIZE], std::string moveTo);
public:
	King(std::string loc, bool color);
	bool illegalMove(std::string moveTo); //returns true if the move is ilegal
	bool isPieceInWay(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo);
	virtual char getType() const;
	virtual void setLoc(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE]);
};

