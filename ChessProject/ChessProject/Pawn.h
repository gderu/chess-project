#pragma once
#include "Piece.h"
class Pawn : public Piece
{
private:
	bool canCaptureLeft;
	bool canCaptureRight;
	bool movingLegal(int amount, int goalY, int origY);
	bool canEnPassent(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo, bool right);
public:
	//no need for = operator, no pointers
	Pawn(std::string loc, bool color);
	void updateCanCaptureLeft(Piece* boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo);
	void updateCanCaptureRight(Piece* boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo);
	virtual bool isPieceInWay(Piece* boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo);//checks if a piece is in the way between the piece and its goal
	virtual bool illegalMove(std::string moveTo);//checks if the piece can make the move, regardless of the current board
	virtual void setLoc(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE]) override;//sets the location using the Piece function, but also increases moveNum by 1 as well as chekcin for enpassent
	virtual Piece * setLocNoDel(std::string moveTo, Piece * board[BOARD_SIZE][BOARD_SIZE], Piece * replaceWith) override;
	virtual char getType() const;
};

