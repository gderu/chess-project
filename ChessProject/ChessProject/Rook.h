#pragma once
#include "Piece.h"

class Rook: virtual public Piece
{
public:
	Rook(std::string loc, bool color);
	bool illegalMove(std::string moveTo); //returns true if the move is ilegal
	bool isPieceInWay(Piece * boardRep[BOARD_SIZE][BOARD_SIZE], std::string moveTo);
	virtual char getType() const;
};

