#include "Test.h"


Test::Test()
{
	for (unsigned int i = 0; i < BOARD_SIZE; i++)
	{
		for (unsigned int k = 0; k < BOARD_SIZE; k++)
		{
			_boardRep[i][k] = nullptr;
		}
	}
}


Test::~Test()
{
	for (unsigned int i = 0; i < BOARD_SIZE; i++)
	{
		for (unsigned int k = 0; k < BOARD_SIZE; k++)
		{
			if (_boardRep[i][k] != nullptr)
			{
				_boardRep[i][k] = nullptr;
			}
		}
	}
}

void Test::cleanBoard()
{
	for (int i = 0; i < BOARD_SIZE; i++) {
		for (int j = 0; j < BOARD_SIZE; j++) {
			this->_boardRep[i][j] = nullptr;
		}
	}
}

void Test::printBoard()
{
	using namespace std;
	for (unsigned int i = 0; i < BOARD_SIZE; i++)
	{
		for (unsigned int k = 0; k < BOARD_SIZE; k++)
		{
			if (nullptr == _boardRep[i][k])
			{
				cout << " X  ";
			}
			else
			{
				cout << " " <<_boardRep[i][k]->getColor() << "  ";
			}
		}
		cout <<endl;
	}
}

void Test::testPawn()
{
	using namespace std;
	this->cleanBoard();
	_boardRep[0][0] = new Pawn(std::string("a1"), false);
	_boardRep[2][0] = new Pawn(std::string("a3"), false);
	if (!_boardRep[0][0]->illegalMove("a3"))//illegalMoves must be taken care of before in the way.
	{
		cout << "good" << endl;
	}
	else
	{
		cout << "bad" << endl;
	}
	if (!_boardRep[0][0]->illegalMove("a2")) //sideways, bad
	{
		cout << "good2" << endl;
	}
	else
	{
		cout << "bad2" << endl;
	}
	if (_boardRep[0][0]->illegalMove("a4"))
	{
		cout << "good3" << endl;
	}
	else
	{
		cout << "bad3" << endl;
	}
	if (!(_boardRep[0][0]->isPieceInWay(_boardRep, "a2")))//notihing in the way
	{
		cout << "good4" << endl;
	}
	else
	{
		cout << "bad4" << endl;
	}
	if (_boardRep[0][0]->isPieceInWay(_boardRep, "a3"))  //sideways, bad
	{
		cout << "good5" << endl;
	}
	else
	{
		cout << "bad5" << endl;
	}
	delete _boardRep[0][0];
}

void Test::testQueen()
{
	using namespace std;
	this->cleanBoard();
	_boardRep[0][0] = (Piece*)(new Queen(std::string("a1"), false));
	_boardRep[1][1] = (Piece*)(new Queen(std::string("b2"), false));
	if (!_boardRep[0][0]->illegalMove("a7"))//illegalMoves must be taken care of before in the way.
	{
		cout << "good" << endl;
	}
	else
	{
		cout << "bad" << endl;
	}
	if (!_boardRep[0][0]->illegalMove("c3")) //sideways, bad
	{
		cout << "good2" << endl;
	}
	else
	{
		cout << "bad2" << endl;
	}
	if (_boardRep[0][0]->isPieceInWay(_boardRep, "c3"))
	{
		cout << "good3" << endl;
	}
	else
	{
		cout << "bad3" << endl;
	}

	if (_boardRep[0][0]->illegalMove("c2"))//notihing in the way
	{
		cout << "good4" << endl;
	}
	else
	{
		cout << "bad4" << endl;
	}
	if (!_boardRep[0][0]->isPieceInWay(_boardRep, "c1"))  //sideways, bad
	{
		cout << "good5" << endl;
	}
	else
	{
		cout << "bad5" << endl;
	}
	delete _boardRep[0][0];
	delete _boardRep[1][1];
}

void Test::testPiece() {
	using namespace std;
	this->cleanBoard();
	vector<Piece*> t;
	_boardRep[0][0] = new Rook(std::string("a1"), false);
	if (_boardRep[0][0]->getLoc() == string("a1"))  //sideways, bad
	{
		cout << "good6" << endl;
	}
	else
	{
		cout << "bad6" << endl;
	}
	_boardRep[0][1] = nullptr;
	_boardRep[0][0]->setLoc(string("b1"), _boardRep);
	if (_boardRep[0][1] != nullptr && _boardRep[0][1]->getLoc() == string("b1")) {
		cout << "good7" << endl;
	}
	else
	{
		cout << "bad7" << endl;
	}
	delete _boardRep[0][1];
}

void Test::testRook()
{
	using namespace std;
	this->cleanBoard();
	_boardRep[0][0] = new Rook(std::string("a1"), false);
	_boardRep[4][0] = new Rook(std::string("a5"), false);
	_boardRep[0][1] = new Rook(std::string("b1"), false);
	if (! _boardRep[0][0]->illegalMove("a7"))//illegalMoves must be taken care of before in the way.
	{
		cout << "good" << endl;
	}
	else
	{
		cout << "bad" << endl;
	}
	if (_boardRep[0][0]->illegalMove("b7")) //sideways, bad
	{
		cout << "good2" << endl;
	}
	else
	{
		cout << "bad2" << endl;
	}
	if (_boardRep[0][0]->isPieceInWay(_boardRep, "a7"))
	{
		cout << "good3" << endl;
	}
	else
	{
		cout << "bad3" << endl;
	}

	if (! (_boardRep[0][0]->isPieceInWay(_boardRep, "a3")))//notihing in the way
	{
		cout << "good4" << endl;
	}
	else
	{
		cout << "bad4" << endl;
	}
	if (_boardRep[0][0]->isPieceInWay(_boardRep, "c1"))  //sideways, bad
	{
		cout << "good5" << endl;
	}
	else
	{
		cout << "bad5" << endl;
	}
	delete _boardRep[0][0];
	delete _boardRep[4][0];
	delete _boardRep[0][1];
}

void Test::testBishop()
{
	using namespace std;
	this->cleanBoard();
	_boardRep[1][1] = new Bishop(std::string("b2"), false);
	_boardRep[0][0] = new Bishop(std::string("a1"), true);
	_boardRep[2][2] = new Bishop(std::string("c3"), false);
	if (_boardRep[1][1]->illegalMove("a7"))//illegalMoves must be taken care of before in the way.
	{
		cout << "good" << endl;
	}
	else
	{
		cout << "bad" << endl;
	}
	if (!_boardRep[1][1]->illegalMove("c1")) //sideways, bad
	{
		cout << "good2" << endl;
	}
	else
	{
		cout << "bad2" << endl;
	}
	if (!_boardRep[1][1]->isPieceInWay(_boardRep, "a1"))
	{
		cout << "good3" << endl;
	}
	else
	{
		cout << "bad3" << endl;
	}

	if ((_boardRep[1][1]->isPieceInWay(_boardRep, "d4")))//notihing in the way
	{
		cout << "good4" << endl;
	}
	else
	{
		cout << "bad4" << endl;
	}
	if (!_boardRep[1][1]->isPieceInWay(_boardRep, "c1"))  //sideways, bad
	{
		cout << "good5" << endl;
	}
	else
	{
		cout << "bad5" << endl;
	}
	delete _boardRep[1][1];
	delete _boardRep[0][0];
	delete _boardRep[2][2];
}

void Test::testKing()
{
	using namespace std;
	this->cleanBoard();
	_boardRep[0][0] = new King(std::string("a1"), false);
	_boardRep[1][0] = new King(std::string("a2"), false);
	if (_boardRep[0][0]->illegalMove("a7"))//illegalMoves must be taken care of before in the way.
	{
		cout << "good" << endl;
	}
	else
	{
		cout << "bad" << endl;
	}
	if (_boardRep[0][0]->illegalMove("b7")) //sideways, bad
	{
		cout << "good2" << endl;
	}
	else
	{
		cout << "bad2" << endl;
	}
	if (! _boardRep[0][0]->isPieceInWay(_boardRep, "b1")) //nothing in the way
	{
		cout << "good3" << endl;
	}
	else
	{
		cout << "bad3" << endl;
	}
	if (_boardRep[0][0]->isPieceInWay(_boardRep, "a2")) //one at the end
	{
		cout << "good4" << endl;
	}
	else
	{
		cout << "bad4" << endl;
	}
	delete _boardRep[0][0];
	delete _boardRep[1][0];
}

void Test::testKnight()
{
	using namespace std;
	this->cleanBoard();
	_boardRep[0][0] = new Knight(std::string("a1"), false);
	_boardRep[1][0] = new Knight(std::string("a2"), false);
	_boardRep[0][1] = new Knight(std::string("b1"), false);
	_boardRep[2][1] = new Knight(std::string("b3"), false);
	_boardRep[1][2] = new Knight(std::string("c2"), true);
	if (_boardRep[0][0]->illegalMove("a7"))//illegalMove
	{
		cout << "good" << endl;
	}
	else
	{
		cout << "bad" << endl;
	}
	if (! _boardRep[0][0]->illegalMove("c2")) //llegalmove
	{
		cout << "good2" << endl;
	}
	else
	{
		cout << "bad2" << endl;
	}
	if (! _boardRep[0][0]->illegalMove("b3")) //llegalmove
	{
		cout << "good3" << endl;
	}
	else
	{
		cout << "bad3" << endl;
	}
	if (!_boardRep[0][0]->isPieceInWay(_boardRep, "c2")) //capture
	{
		cout << "good4" << endl;
	}
	else
	{
		cout << "bad4" << endl;
	}
	if (_boardRep[0][0]->isPieceInWay(_boardRep, "b3")) //can't capture it's own team
	{
		cout << "good5" << endl;
	}
	else
	{
		cout << "bad5" << endl;
	}
	delete _boardRep[0][0];
	delete _boardRep[1][0];
	delete _boardRep[0][1];
	delete _boardRep[2][1];
	delete _boardRep[1][2];
}

